module.exports = {
  //TestingEnvironment: process.env.TEST_ENV || "QA",
  TestingEnvironment: "QA",
  isApplicationAngular: false,
  explicitWait: 60000,


  appURL: {
    QA: 'https://www.google.co.in',
    WebBilling_QA: 'https://qa.personalplans03.intra.mcapp.com/billing'
  },
  testData: {
    title: 'Google',
    searchkey:'selenium',
    username:'yohan123',
    password:'test@123'
  }

};
