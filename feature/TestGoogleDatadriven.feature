@smokePOM
  @smoke
Feature: Test google Search with data sets

  @smokeDataDriven
  Scenario Outline: Test Search with keyword
    Given I navigates to google page with POM
    When I enter search key <searchkey> with POM
    Then I should see results with  <searchkey> using POM

    Examples:
      | searchkey |
      | java |
      | selenium |
    #Then I should see results with key "java" using POM

