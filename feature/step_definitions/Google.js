
var {defineSupportCode} = require("cucumber");
defineSupportCode(function ({Given, Then, When}) {
Given(/^I navigates to google page$/, function (callback) {
    browser.get(globalData.appURL.QA);
    callback();
});
When(/^I enter search key selenium$/, function (callback) {
/*   protractor.browser.sleep(2000);
    element(by.xpath("//input[@name='q']")).sendKeys(globalData.testData.searchkey);
    protractor.browser.sleep(2000);
    protractor.browser.sleep(2000);
    element(by.xpath("//input[@name='q']")).sendKeys(protractor.Key.ENTER);
    protractor.browser.sleep(2000);
    protractor.browser.sleep(2000);*/
    element(by.xpath("//input[@name='q11']")).sendKeys(globalData.testData.searchkey).then(function () {
        element(by.xpath("//input[@name='q']")).sendKeys(protractor.Key.ENTER).then(function () {
            callback();
    });
    });
});
    Then(/^I should see results with key selenium$/, function (callback) {
      element(by.css('div.r>a>h3')).getText().then(function (actual) {
          //expect(text).to.equal('selenium');
          expect(actual.toLowerCase()).contains('selenium');
        callback();
    });
});
});
