var {Then} = require('cucumber');
var {When} = require('cucumber');
var {Given} = require('cucumber');
var loginpage = require('../../pages/WebBilling/LoginPage');

Given(/^I navigates to WebBilling Login Page$/, function (callback) {
    browser.get(globalData.appURL.WebBilling_QA).then(function()
    {
        element(loginpage.loginHeader).getText().then(function (actual) {
            expect(actual.toLowerCase()).contains('web billing login');
        });
    })
    callback();
});

When(/^I enter username and password$/, function (callback) {
  loginpage.loginFunctionality(function()
  {
      callback();
  })
});
Then(/^I should see Delivery Method Page$/, function () {
    
});
When(/^I select the Delivery Method$/, function (callback) {
    
  });
Then(/^I should see verification code page$/, function () {
    console.log("some text");
});
When(/^I enter the verification code$/, function (callback) {
    
});
Then(/^I should see home page of webBilling$/, function () {
    console.log("some text");
});