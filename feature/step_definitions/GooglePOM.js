var {defineSupportCode} = require("cucumber");
var ghomePage = require('../../pages/GoogleHomePage');
var searchPage = require('../../pages/SearchResultsPage');
var {When} = require('cucumber');
var {Then} = require('cucumber');
defineSupportCode(function ({Given, Then, When}) {

    Given(/^I navigates to google page with POM$/, function (callback) {
        browser.get(globalData.appURL.QA);
        callback();
    });
    When(/^I enter search word Java with POM$/, function (callback) {
        ghomePage.search('Java',function () {
            callback();
        })
    });
    Then(/^I should see results with key java using POM$/, function (callback) {
        searchPage.searchResults.getText().then(function (actual) {
            //expect(actual.toLowerCase()).contains('selenium');
            //expect(actual.toLowerCase()).contains('Dummy');
            expect(actual.toLowerCase()).contains('java');
            callback();
    });
});

Then(/^I should see results with key "([^"]*)" using POM$/, function (key,callback) {
    searchPage.searchResults.getText().then(function (actual) {

        expect(actual.toLowerCase()).contains(key);
        callback();
    });
});
    When(/^I enter search key (.*) with POM$/, function (key,callback) {
        ghomePage.search(key,function () {
            callback();
    });
});
    Then(/^I should see results with  (.*) using POM$/, function (key,callback) {
        searchPage.searchResults.getText().then(function (actual) {

            expect(actual.toLowerCase()).contains('selenium');
            callback();
    });
});

});
When(/^I enter search word Java with POM$/, function () {

});
Then(/^I write something$/, function () {

});