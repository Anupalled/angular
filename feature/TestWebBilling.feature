@smoke
Feature: Test WebBilling Application

  @scenario1
  Scenario: Test Login functionality of WebBilling
    Given I navigates to WebBilling Login Page
    When I enter username and password
    Then I should see Delivery Method Page
    When I select the Delivery Method
    Then I should see verification code page
    When I enter the verification code 
    Then I should see home page of webBilling