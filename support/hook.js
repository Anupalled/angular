const {Before, After, Status} = require('cucumber');
const {setDefaultTimeout} = require('cucumber');
Before(function () {
  // console.log('before')
  browser.ignoreSynchronization = false;
  browser.waitForAngularEnabled(false);
  setDefaultTimeout(20 * 1000);

});


After(function (scenario) {
  if (scenario.result.status === 'failed') {
    console.log("in hooks after")
    var attach = this.attach;
    return browser.takeScreenshot().then(function (png) {
      var decodedImage = new Buffer(png, "base64");


      return attach(decodedImage, "image/png");
    });
  }
});


