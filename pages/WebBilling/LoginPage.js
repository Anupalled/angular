var usernameTxtBox = by.xpath("//input[@name='username']");
var passwordTxtBox=by.xpath("//input[@name='password']");
var loginButton=by.xpath("//button[@type='submit']");
var loginHeader=by.xpath("//h3[text()=' Web billing Login']")

function enterUserName(key,callback)
{
    element(usernameTxtBox).sendKeys(key).then(function (){
    callback();
})

}

function enterPassword(key,callback)
{
    element(passwordTxtBox).sendKeys(key).then(function (){
        callback();
    })
}

function clickLoginButton(callback) {
    element(loginButton).click().then(function () {
        callback();
    })

}

function loginFunctionality(callback) {
    element(usernameTxtBox).sendKeys(globalData.testData.username).then(function ()
    {
        element(passwordTxtBox).sendKeys(globalData.testData.password).then(function()
        {
            element(loginButton).click().then(function () {
                callback();
            })
        })
    })

}

module.exports = {
    enterUserName: enterUserName,
    enterPassword: enterPassword,
    clickOnLogin :clickLoginButton,
    loginHeader:loginHeader,
    loginFunctionality:loginFunctionality
};

