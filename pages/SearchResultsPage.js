/**
 * Created by yugandhar-gorrepati on 2/27/2018.
 */

var searchResults = element(by.css("div.r a>h3"));


module.exports = {
  searchResults: searchResults,
};
