/**
 * Created by yugandhar-gorrepati on 2/27/2018.
 */

var searchBox = by.xpath("//input[@name='q']");

function search(key, callback) {
  element(searchBox).sendKeys(key).then(function () {
    element(searchBox).sendKeys(protractor.Key.ENTER).then(function () {
                callback();
    });
  });

}




module.exports = {
  search: search,
};
