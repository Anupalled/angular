
exports.config = {
    directConnect: true,
//Running chrome
    Capabilities: { browserName: 'chrome'
    },
//point spec to feature file , my feature file was under feature folder
    specs: ['feature/TestWebBilling.feature'],
    ignoreSynchronization: true,
    maxInstances: 1,
    ignoreUncaughtExceptions: true,
    allScriptsTimeout: 180000,//This is the overall Timeout
//set framework options
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
//just maximizing window before testing
    onPrepare: function(){

        browser.driver.manage().window().maximize();
        global.globalData = require('./testData/global.js');
        var chai = require('chai');
        var chaiAsPromised = require('chai-as-promised');
        chai.use(chaiAsPromised);
        global.expect = chai.expect;
        global.assert = chai.assert;

    } ,
//Create html report
    onComplete: () => {
        var reporter = require('cucumber-html-reporter');
        var options = {
            theme: 'bootstrap',
            jsonFile: './results.json',
            output: './results.html',
            reportSuiteAsScenarios: true,
            //launchReport: true,
            output: './report/cucumber_report.html',
        };
        reporter.generate(options);
    },
//set cucumber options
    cucumberOpts: {
        require: ['feature/step_definitions/*.js', 'support/hook.js'],
        strict: true,
        format: [],    //don't put 'Pretty' as it is depreciated
        'dry-run': false,
        compiler: [],
        format: 'json:results.json',    //make sure you are not using multi-capabilities
    },

};